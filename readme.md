# Transaction viewer

## <u>About</u>
This is a simple **Spring Boot web application**, which allows to view user transactions categorised by categories of their choosing. All categories have their own category summary, where **Grand Total** and **Average Monthly Spendings** are shown. 

The user also has an option to view an annual report for each category by pressing **Annual Spending Reports** button. There, a list of annual periods is shown that the transactions are available for. The annual report view gives the user the information about their **Maximum and Minimum Spending** throughout the year, as well as the **Total Spedning** in that period. 

The category of a transaction can be changed only from **Category Transactrions View**, where the user can specify a category that the transaction will be reasigned to. 

## <u>How to use</u>
Clone GIT repository to your directory of choice and go to target directory. This directory contains already build .jar file which is run by the following command: 
```
java -jar transaction_viewer-1.0.0-SNAPSHOT.jar
```
After the jar is running, please go to the following URL: http://localhost:8085/categories to start using this application.
In order to close the application press ctrl+c in the command prompt.

## <u>Sample data</u>
The sample data is being defined as a set of insert statements in data.sql in resources directory. This file is being read at the application start-up, and the statments are being exeucted, which loads the data to the in-memory database. The sample data consist of three years of typical transactions, to help model an average person’s account. Transactions without specified category are automatically assigend to 'Other' category by default. 

## <u>Technology and libraries</u>
This application is based on Spring framework, which is by far the most popular framework for developing applications in Java. The core module used in this application is **Spring Boot**, which is a starter module for providing core functionality. The other Spring modules used in this application are **Spring Web, Spring JPA and Spring Thymeleaf**. The application also has **H2 in-memory database**, which acts as storage solution for sample data. The database can be viewed by accesing the following URL http://localhost:8085/h2-console and clicking 'Connect'. 

**Spring Web** module is used to start emmbedded Tomcat server, which hosts the pages used for displaying the data. More importantly is provides a set of useful annotations that help to configure different request endpoints. 

**Spring JPA** provides a highly abstracted and simplified way of implementing data-access layer using Java Persistance API. By using annotations, it completley eliminates the boiler-plate code, which allows the developer to focus on implementation of business logic. 

**Spring Thymeleaf** is the module that provides the support to access data from templates. Thymeleaf is a modern server-side Java template engine for both web and standalone environments. What Spring Thymeleaf does is that it bridges the gap between the controller and the view in MVC (Model-view-controller) application, through the use of model attributes, which bind data to specific attributes, which in turn can be accessed in the template.