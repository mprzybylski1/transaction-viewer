package com.example.transaction_viewer.services;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

import java.sql.Date;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import com.example.transaction_viewer.models.AnnualPeriod;
import com.example.transaction_viewer.models.AnnualSummary;
import com.example.transaction_viewer.models.Category;
import com.example.transaction_viewer.models.CategorySummary;
import com.example.transaction_viewer.models.Transaction;
import com.example.transaction_viewer.repositories.TransactionRepository;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;

@SpringBootTest
@ExtendWith(MockitoExtension.class)
public class TransactionServiceTests{

    @Autowired
    @InjectMocks
    private TransactionService transactionService;

    @MockBean
    private TransactionRepository transactionRepository;

    @Test
    public void ShouldCalculateCorrectCategorySummary(){
        List<Transaction> transactions = new ArrayList<>();

        Transaction transactionMock1 = mock(Transaction.class);
        when(transactionMock1.getAmount()).thenReturn(20d);
        when(transactionMock1.getDatestamp()).thenReturn(Date.valueOf(LocalDate.now().minusMonths(1l)));

        Transaction transactionMock2 = mock(Transaction.class);
        when(transactionMock2.getAmount()).thenReturn(10d);
        when(transactionMock2.getDatestamp()).thenReturn(Date.valueOf(LocalDate.now()));

        transactions.add(transactionMock1);
        transactions.add(transactionMock2);

        when(transactionRepository.findAllByCategory(any())).thenReturn(transactions);
        when(transactionRepository.findFirstByCategoryOrderByDatestampAsc(any())).thenReturn(Optional.of(transactionMock1));
        when(transactionRepository.findFirstByCategoryOrderByDatestampDesc(any())).thenReturn(Optional.of(transactionMock2));

        Category categoryMock = mock(Category.class);

        CategorySummary categorySummary = transactionService.getCategorySummary(categoryMock);
        assertNotNull(categorySummary);
        assertEquals(15d, categorySummary.getAverageMonthlySpend());
        assertEquals(30d, categorySummary.getGrandTotal());
    }

    @Test
    public void ShouldCalculateCorrectCategorySummaryForOneTransaction(){
        List<Transaction> transactions = new ArrayList<>();

        Transaction transactionMock1 = mock(Transaction.class);
        when(transactionMock1.getAmount()).thenReturn(20d);

        transactions.add(transactionMock1);

        when(transactionRepository.findAllByCategory(any())).thenReturn(transactions);
        when(transactionRepository.findFirstByCategoryOrderByDatestampAsc(any())).thenReturn(Optional.of(transactionMock1));

        Category categoryMock = mock(Category.class);
        CategorySummary categorySummary = transactionService.getCategorySummary(categoryMock);
        assertEquals(20d, categorySummary.getGrandTotal());
        assertEquals(0d, categorySummary.getAverageMonthlySpend());
    }

    @Test
    public void ShouldCalculateCorrectAnnualSummary(){
        List<Transaction> transactions = new ArrayList<>();

        Transaction transactionMock1 = mock(Transaction.class);
        when(transactionMock1.getAmount()).thenReturn(20d);

        Transaction transactionMock2 = mock(Transaction.class);
        when(transactionMock2.getAmount()).thenReturn(10d);

        transactions.add(transactionMock1);
        transactions.add(transactionMock2);

        Category categoryMock = mock(Category.class);
        AnnualPeriod periodMock = mock(AnnualPeriod.class);

        when(transactionRepository.findAllByCategoryAndPeriod(any(), any())).thenReturn(transactions);

        AnnualSummary annualSummary = transactionService.getAnnualSummary(categoryMock, periodMock);
        assertNotNull(annualSummary);
        assertEquals(20d, annualSummary.getMaxSpend());
        assertEquals(10d, annualSummary.getMinSpend());
        assertEquals(30d, annualSummary.getTotalSpend());
    }

    @Test
    public void ShouldCalculateAnnualSummaryForNoTransactions(){
        List<Transaction> transactions = new ArrayList<>();

        when(transactionRepository.findAllByCategoryAndPeriod(any(), any())).thenReturn(transactions);

        Category categoryMock = mock(Category.class);
        AnnualPeriod periodMock = mock(AnnualPeriod.class);

        AnnualSummary annualSummary = transactionService.getAnnualSummary(categoryMock, periodMock);
        assertNotNull(annualSummary);
        assertEquals(0d, annualSummary.getMaxSpend());
        assertEquals(0d, annualSummary.getMinSpend());
        assertEquals(0d, annualSummary.getTotalSpend());
    }

}