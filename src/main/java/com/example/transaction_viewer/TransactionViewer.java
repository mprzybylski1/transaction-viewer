package com.example.transaction_viewer;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class TransactionViewer {

	public static void main(String[] args) {
		SpringApplication.run(TransactionViewer.class, args);
	}

}
