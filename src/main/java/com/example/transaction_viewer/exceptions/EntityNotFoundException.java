package com.example.transaction_viewer.exceptions;

public class EntityNotFoundException extends Exception{

    private static final long serialVersionUID = 8334847548623035010L;

    public EntityNotFoundException() {
        super();
    }

    public EntityNotFoundException(String msg){
        super(msg);
    }
    
}
