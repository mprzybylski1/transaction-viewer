package com.example.transaction_viewer.exceptions;

public class DataIntegrityException extends Exception {

    private static final long serialVersionUID = 5653484335906595179L;

    public DataIntegrityException() {
        super();
    }

    public DataIntegrityException(String msg){
        super(msg);
    }
    
}
