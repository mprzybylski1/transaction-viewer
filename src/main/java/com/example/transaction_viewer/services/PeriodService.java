package com.example.transaction_viewer.services;

import java.util.List;
import java.util.Optional;

import com.example.transaction_viewer.models.AnnualPeriod;
import com.example.transaction_viewer.repositories.PeriodRepository;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class PeriodService {
    
    @Autowired
    private PeriodRepository periodRepository;

    public List<AnnualPeriod> findAll(){
        return periodRepository.findAll();
    }

    public Optional<AnnualPeriod> findByName(String period){
        return periodRepository.findByPeriod(period);
    }

}
