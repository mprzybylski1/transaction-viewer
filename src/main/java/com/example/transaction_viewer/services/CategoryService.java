package com.example.transaction_viewer.services;

import java.util.List;
import java.util.Optional;

import com.example.transaction_viewer.exceptions.DataIntegrityException;
import com.example.transaction_viewer.models.Category;
import com.example.transaction_viewer.repositories.CategoryRepository;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.stereotype.Service;

@Service
public class CategoryService {

    private Logger logger = LoggerFactory.getLogger(CategoryService.class);

    @Autowired
    private CategoryRepository categoryRepository;

    public List<Category> findAll(){
        return categoryRepository.findAll();
    }

    public Category saveCategory(String category) throws DataIntegrityException {
        try{
            return categoryRepository.save(new Category(category));
        }catch(DataIntegrityViolationException e){
            String msg = "Unique constraint violated - Cannot save category: " + category;
            logger.error(msg, e);
            throw new DataIntegrityException(msg);
        }
    }

    public Optional<Category> findByCategoryName(String categoryName){
        return categoryRepository.findByCategoryName(categoryName);
    }
    
}
