package com.example.transaction_viewer.services;

import java.time.Period;
import java.util.List;
import java.util.stream.Collectors;

import javax.transaction.Transactional;

import com.example.transaction_viewer.exceptions.EntityNotFoundException;
import com.example.transaction_viewer.models.AnnualPeriod;
import com.example.transaction_viewer.models.AnnualSummary;
import com.example.transaction_viewer.models.Category;
import com.example.transaction_viewer.models.CategorySummary;
import com.example.transaction_viewer.models.Transaction;
import com.example.transaction_viewer.repositories.TransactionRepository;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class TransactionService {

    private Logger logger = LoggerFactory.getLogger(TransactionService.class);

    @Autowired
    private TransactionRepository trxRepository;

    public List<Transaction> findAll(){
        return trxRepository.findAll();
    }

    public CategorySummary getCategorySummary(Category category){
        return calculateCategorySummary(category);
    }

    public Transaction findById(long id) {
        return trxRepository.findById(id).orElse(null);
    }

    public List<Transaction> findAllByCategory(Category category){
        return trxRepository.findAllByCategoryOrderByDatestampDesc(category);
    }

    public List<Transaction> findAllByCategoryAndPeriod(Category category, AnnualPeriod period){
        return trxRepository.findAllByCategoryAndPeriod(category, period);
    }

    private CategorySummary calculateCategorySummary(Category category){
        CategorySummary categorySummary = new CategorySummary();

        categorySummary.setGrandTotal(calculateCatgoryGrandTotal(category));
        categorySummary.setAverageMonthlySpend(calculateCategoryMonthlyAverageSpend(category));

        return categorySummary;
    }

    public List<String> findAllPeriodsForCategory(Category category){
        return trxRepository.findAllByCategory(category).stream().map(Transaction::getPeriod).distinct().collect(Collectors.toList());
    }

    private double calculateCatgoryGrandTotal(Category category){
        return trxRepository.findAllByCategory(category).stream().map(Transaction::getAmount).reduce(0d, Double::sum);
    }

    private double calculateCategoryMonthlyAverageSpend(Category category){
        double grandTotal = calculateCatgoryGrandTotal(category);

        try{
            Transaction firstTransaction = trxRepository.findFirstByCategoryOrderByDatestampAsc(category).orElseThrow(
                () -> new EntityNotFoundException("Could not find first transaction for the following category: " + category));

            Transaction lastTransaction = trxRepository.findFirstByCategoryOrderByDatestampDesc(category).orElseThrow(
                () -> new EntityNotFoundException("Could not find last transaction for the following category: " + category));

            Period difference = Period.between(firstTransaction.getDatestamp().toLocalDate().withDayOfMonth(1), lastTransaction.getDatestamp().toLocalDate().withDayOfMonth(1));
            return grandTotal / (difference.toTotalMonths() + 1);
        }catch(EntityNotFoundException e){
            logger.error("Could not calculate average monthly spend", e);
            return 0d;
        }
    }

    public AnnualSummary getAnnualSummary(Category category, AnnualPeriod period){
        List<Transaction> transactions = trxRepository.findAllByCategoryAndPeriod(category, period);

        AnnualSummary annualSummary = new AnnualSummary();
        annualSummary.setPeriod(period);
        annualSummary.setTotalSpend(transactions.stream().map(Transaction::getAmount).reduce(0d, Double::sum));
        annualSummary.setMaxSpend(transactions.stream().map(Transaction::getAmount).max(Double::compare).orElse(0d));
        annualSummary.setMinSpend(transactions.stream().map(Transaction::getAmount).min(Double::compare).orElse(0d));

        return annualSummary;
    }

    @Transactional
    public void updateTransactionCategory(long transactionId, Category category) {
        trxRepository.updateCategory(transactionId, category.getCategoryId());
    }
    
}
