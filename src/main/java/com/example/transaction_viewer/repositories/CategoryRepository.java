package com.example.transaction_viewer.repositories;

import java.util.Optional;

import com.example.transaction_viewer.models.Category;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface CategoryRepository extends JpaRepository<Category, Long>{

    public Optional<Category> findByCategoryName(String categoryName);
    

}
