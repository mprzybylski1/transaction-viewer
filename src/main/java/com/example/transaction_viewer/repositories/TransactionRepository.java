package com.example.transaction_viewer.repositories;

import java.util.List;
import java.util.Optional;

import com.example.transaction_viewer.models.AnnualPeriod;
import com.example.transaction_viewer.models.Category;
import com.example.transaction_viewer.models.Transaction;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

@Repository
public interface TransactionRepository extends JpaRepository<Transaction, Long>{

	public List<Transaction> findAll();

	public List<Transaction> findAllByCategory(Category category);

	public List<Transaction> findAllByCategoryOrderByDatestampDesc(Category category);

	public Optional<Transaction> findById(long id);

	public List<Transaction> findAllByCategoryAndPeriod(Category category, AnnualPeriod period);

	public Optional<Transaction> findFirstByCategoryOrderByDatestampDesc(Category category);

	public Optional<Transaction> findFirstByCategoryOrderByDatestampAsc(Category category);

	@Modifying
	@Query(value = "update transactions t set t.category_id = :categoryId where t.transaction_id = :transactionId", nativeQuery = true)
	public void updateCategory(long transactionId, long categoryId);

}
