package com.example.transaction_viewer.repositories;

import java.util.Optional;

import com.example.transaction_viewer.models.AnnualPeriod;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface PeriodRepository extends JpaRepository<AnnualPeriod, Long>{
    
    public Optional<AnnualPeriod> findByPeriod(String periodName);
}
