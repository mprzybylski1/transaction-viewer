package com.example.transaction_viewer.models;

public class CategorySummary {
    
    private double grandTotal;
    private double averageMonthlySpend;

    public double getGrandTotal() {
        return grandTotal;
    }

    public void setGrandTotal(double grandTotal) {
        this.grandTotal = grandTotal;
    }

    public double getAverageMonthlySpend() {
        return averageMonthlySpend;
    }

    public void setAverageMonthlySpend(double averageMonthlySpend) {
        this.averageMonthlySpend = averageMonthlySpend;
    }
    
}
