package com.example.transaction_viewer.models;

public class AnnualSummary {

    private AnnualPeriod period;
    private double totalSpend;
    private double maxSpend;
    private double minSpend;

    public String getPeriod() {
        return period.getPeriod();
    }

    public void setPeriod(AnnualPeriod period) {
        this.period = period;
    }

    public double getTotalSpend() {
        return totalSpend;
    }

    public void setTotalSpend(double totalSpend) {
        this.totalSpend = totalSpend;
    }

    public double getMaxSpend() {
        return maxSpend;
    }

    public void setMaxSpend(double maxSpend) {
        this.maxSpend = maxSpend;
    }

    public double getMinSpend() {
        return minSpend;
    }

    public void setMinSpend(double minSpend) {
        this.minSpend = minSpend;
    }
    
}
