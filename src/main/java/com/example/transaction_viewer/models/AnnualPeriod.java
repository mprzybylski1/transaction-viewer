package com.example.transaction_viewer.models;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "periods")
public class AnnualPeriod {
    
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "period_id")
    private long periodId;

    @Column(name = "period", nullable = false)
    private String period;

    public AnnualPeriod(){
    
    }

    public AnnualPeriod(long periodId, String period){
        this.periodId = periodId;
        this.period = period;
    }

    public long getPeriodId() {
        return periodId;
    }

    public void setPeriodId(long periodId) {
        this.periodId = periodId;
    }

    public String getPeriod() {
        return period;
    }

    public void setPeriod(String period) {
        this.period = period;
    }
    
}
