package com.example.transaction_viewer.models;

import java.sql.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;

@Entity
@Table(name = "transactions")
public class Transaction{

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "transaction_id")
    private long transactionId;

    @Column(name = "vendor", nullable = false)
    private String vendor;

    @Column(name = "type", nullable = false)
    private String type;

    @Column(name = "amount", nullable = false)
    private double amount;
    
    @Column(name = "datestamp", nullable = false)
    private Date datestamp;

    @OneToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "category_Id", columnDefinition = "int default select category_id from categories where category_name = 'Other'")
    private Category category;

    @OneToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "period_Id", nullable = false)
    private AnnualPeriod period;

    public long getTransactionId() {
        return transactionId;
    }

    public void setTransactionId(long transactionId) {
        this.transactionId = transactionId;
    }

    public String getVendor() {
        return vendor;
    }

    public void setVendor(String vendor) {
        this.vendor = vendor;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getCategoryName() {
        return this.category.getCategoryName();
    }

    public void setCategory(Category category) {
        this.category = category;
    }

    public Date getDatestamp() {
        return datestamp;
    }

    public void setDatestamp(Date datestamp) {
        this.datestamp = datestamp;
    }

    public double getAmount() {
        return amount;
    }

    public void setAmount(double amount) {
        this.amount = amount;
    }

    public String getPeriod() {
        return period.getPeriod();
    }

    public void setPeriod(AnnualPeriod period) {
        this.period = period;
    }

    @Override
    public String toString() {
        return "Transaction [amount=" + amount + ", categoryName=" + getCategoryName() + ", datestamp=" + datestamp
                + ", period=" + period + ", transactionId=" + transactionId + ", type=" + type + ", vendor=" + vendor
                + "]";
    }

}