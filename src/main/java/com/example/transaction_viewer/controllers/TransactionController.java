package com.example.transaction_viewer.controllers;

import java.util.Optional;

import javax.management.InvalidAttributeValueException;
import javax.servlet.http.HttpServletRequest;

import com.example.transaction_viewer.exceptions.DataIntegrityException;
import com.example.transaction_viewer.models.AnnualPeriod;
import com.example.transaction_viewer.models.Category;
import com.example.transaction_viewer.services.CategoryService;
import com.example.transaction_viewer.services.PeriodService;
import com.example.transaction_viewer.services.TransactionService;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.lang.NonNull;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

@Controller
public class TransactionController {

    private Logger logger = LoggerFactory.getLogger(TransactionController.class);

    //Model attributes constants
    private static final String TRANSACTIONS_ATTRIBUTE = "transactions";
    private static final String TRANSACTION_ATTRIBUTE = "transaction";
    private static final String TRANSACTION_ID_ATTRIBUTE = "transaction_id";
    private static final String CATEGORIES_ATTRIBUTE = "categories";
    private static final String CATEGORY_NAME_ATTRIBUTE = "categoryName";
    private static final String PERIODS_ATTRIBUTE = "periods";

    //Template views constants
    private static final String DEFAULT_ERROR_VIEW = "errorView";
    private static final String CAT_ANNNUAL_REPORT_VIEW = "categoryAnnualReportView";
    private static final String ANNNUAL_REPORTS_VIEW = "annualReportsView";
    private static final String EDIT_CAT_VIEW = "editCategoryView";
    private static final String CAT_TRANSACTIONS_VIEW = "categoryTransactionsView";
    private static final String CAT_VIEW = "categoriesView";

    @Autowired
    private TransactionService transactionService;
    @Autowired
    private CategoryService categoryService;
    @Autowired
    private PeriodService periodService;

    @GetMapping("/annual_reports/{category}/annual_report")
    public String showCategoryAnnualReport(@PathVariable("category") String categoryName, @RequestParam("period") String periodName, Model model){
        
        logger.info("Showing annual report for category: {} for period: {}", categoryName, periodName);

        Optional<Category> category = categoryService.findByCategoryName(categoryName);
        Optional<AnnualPeriod> period = periodService.findByName(periodName);

        if(category.isPresent() && period.isPresent()){
            logger.debug("Both category and period found in database - Setting model attributes");

            model.addAttribute("annualSummary", transactionService.getAnnualSummary(category.get(), period.get()));
            model.addAttribute(TRANSACTIONS_ATTRIBUTE, transactionService.findAllByCategoryAndPeriod(category.get(), period.get()));
        }else{
            logger.debug("Could not find category and period in database - Setting model attributes to null");

            model.addAttribute("annualSummary", null);
            model.addAttribute(TRANSACTIONS_ATTRIBUTE, null);
        }

        return CAT_ANNNUAL_REPORT_VIEW;
    }

    @GetMapping("/annual_reports/{category}")
    public String showAnnualReports(@PathVariable("category") String categoryName, Model model){

        logger.info("Showing annual periods for the following category: {}", categoryName);

        Optional<Category> optionalCategory = categoryService.findByCategoryName(categoryName);
        if(optionalCategory.isPresent()){
            logger.debug("Category found in database - Setting model attributes");

            model.addAttribute(PERIODS_ATTRIBUTE, transactionService.findAllPeriodsForCategory(optionalCategory.get()));
        }else{
            logger.debug("Could not find category in database - Setting model attribute to null");

            model.addAttribute(PERIODS_ATTRIBUTE, null);
        }

        return ANNNUAL_REPORTS_VIEW;
    }

    @PostMapping("/categories/{category}/transactions/{transactionId}/edit_category")
    public String saveTransactionCategory(@PathVariable("category") @NonNull String categoryName, 
            @PathVariable("transactionId") @NonNull long transactionId, @RequestParam("new_category") @NonNull String newCategoryName, 
            Model model) throws InvalidAttributeValueException, DataIntegrityException {

        if(newCategoryName.isEmpty()){
            throw new InvalidAttributeValueException();
        }

        logger.info("Received a request to update category of transaction ID: {} to {}", transactionId, newCategoryName);

        Optional<Category> optionalNewCategory = categoryService.findByCategoryName(newCategoryName);
        if(!optionalNewCategory.isPresent()){
            logger.debug("Category is not present in the database - Saving the new category and updating transaction");

            transactionService.updateTransactionCategory(transactionId, categoryService.saveCategory(newCategoryName));
        }else{
            logger.debug("Category is present in the database - Updating transaction");

            transactionService.updateTransactionCategory(transactionId, optionalNewCategory.get());
        }

        return showCategories(model);
    }

    @GetMapping("/categories/{category}/transactions/{transactionId}/edit_category")
    public String editTransactionCategory(@PathVariable("transactionId") long transactionId, Model model){

        logger.info("Showing a view to edit category for transaction ID: {}", transactionId);
        
        model.addAttribute(TRANSACTION_ID_ATTRIBUTE, transactionId);
        model.addAttribute(TRANSACTION_ATTRIBUTE, transactionService.findById(transactionId));
        model.addAttribute(CATEGORIES_ATTRIBUTE, categoryService.findAll());

        return EDIT_CAT_VIEW;
    }

    @GetMapping("/categories/{category}/transactions")
    public String showCategoryTransactions(@PathVariable("category") String categoryName,  Model model) {
        
        logger.info("Showing summary and all transactions for category: {}", categoryName);

        Optional<Category> optionalCategory = categoryService.findByCategoryName(categoryName);
        if(optionalCategory.isPresent()){
            logger.debug("Category is present in database - Setting model attributes");

            model.addAttribute(CATEGORY_NAME_ATTRIBUTE, categoryName);
            model.addAttribute(TRANSACTIONS_ATTRIBUTE, transactionService.findAllByCategory(optionalCategory.get()));
            model.addAttribute("categorySummary", transactionService.getCategorySummary(optionalCategory.get()));
        }

        return CAT_TRANSACTIONS_VIEW;
    }

    @GetMapping("/categories")
    public String showCategories(Model model) {

        logger.info("Showing all categories");

        model.addAttribute(CATEGORIES_ATTRIBUTE, categoryService.findAll());
        return CAT_VIEW;
    }

    @ExceptionHandler(DataIntegrityException.class)
    public ModelAndView dataIntegrityError(HttpServletRequest req, Exception ex) {

        ModelAndView mav = new ModelAndView();
        mav.addObject("reason", "Cannot create a new category with the same name");
        mav.setViewName(DEFAULT_ERROR_VIEW);

        return mav;
    }

    @ExceptionHandler(InvalidAttributeValueException.class)
    public ModelAndView invalidAttributeError(HttpServletRequest req, Exception ex) {

        ModelAndView mav = new ModelAndView();
        mav.addObject("reason", "Cannot create a new category with empty name");
        mav.setViewName(DEFAULT_ERROR_VIEW);

        return mav;
    }

}
